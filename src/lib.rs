use num_traits;
use std::{ f32, f64 };

/// Returns the next representable float value in the direction of y
///
/// Base assumptions:
/// self == y -> return y
/// self >= infinity -> return infinity
/// self <= negative infinity -> return negative infinity
/// self == NaN -> return NaN
///
/// # Examples
///
/// ```
/// use float_next_after::NextAfter;
///
/// // Large numbers
/// let big_num = 16237485966.00000437586943_f64;
/// let next = big_num.next_after(&std::f64::INFINITY);
/// assert_eq!(next, 16237485966.000006_f64);
///
/// // Expected handling of 1.0
/// let one = 1_f64;
/// let next = one.next_after(&std::f64::INFINITY);
/// assert_eq!(next, 1_f64 + std::f64::EPSILON);
///
/// // Tiny (negative) numbers
/// let zero = 0_f32;
/// let next = zero.next_after(&std::f32::NEG_INFINITY);
/// assert_eq!(next, -0.000000000000000000000000000000000000000000001_f32);
///
/// // Equal source/dest (even -0 == 0)
/// let zero = 0_f64;
/// let next = zero.next_after(&-0_f64);
/// assert_eq!(next, -0_f64);
/// ```
///
/// # Safety
///
/// This trait uses the ToBits and FromBits functions from f32 and f64.
/// Those both use unsafe { mem::transmute(self) } / unsafe { mem::transmute(v) }
/// to convert a f32/f64 to u32/u64.  The docs for those functions claim they are safe
/// and that "the safety issues with sNaN were overblown!"
///
pub trait NextAfter<T: num_traits::Float> {
    fn next_after(&self, y: &T) -> T;
}

// Note: I made separate implementations for f32 and f64 in order to make the bit
// conversion explicit to anyone else who might come along to look at this. The f64
// is converted to u64, incremented by one, then converted back to f64. The f32
// is converted to u32, incremented by one, then converted back to f32.
impl NextAfter<f64> for f64 {
    fn next_after(&self, y: &f64) -> f64 {
        if let Some(out) = return_invalid_next(self, y) {
            return out;
        }

        let float_zero = 0_f64;

        return if *self > float_zero || (*self == float_zero && *y > *self) { // Next after for a number greater than 0 is simple
            // Return will be the old trick f64 -> u64 +- 1 -> f64
            if *y > *self {
                f64::from_bits(self.to_bits() + 1)
            } else {
                f64::from_bits(self.to_bits() - 1)
            }
        } else { // Self is negative or 0 and moving negative
            // Get positive representation of x and destination
            let source = if *self == float_zero { float_zero } else { -*self };
            let dest = -*y;

            // Return will be the old trick f64 -> u64 +- 1 -> f64 (but we make it negative)
            if dest > source && source >= float_zero {
                -f64::from_bits(source.to_bits() + 1)
            } else {
                -f64::from_bits(source.to_bits() - 1)
            }
        }
    }
}

impl NextAfter<f32> for f32 {
    fn next_after(&self, y: &f32) -> f32 {
        if let Some(out) = return_invalid_next(self, y) {
            return out;
        }

        let float_zero = 0_f32;

        return if *self > float_zero || (*self == float_zero && *y > *self) { // Next after for a number greater than 0 is simple
            // Return will be the old trick f32 -> u32 +- 1 -> f32
            if *y > *self {
                f32::from_bits(self.to_bits() + 1)
            } else {
                f32::from_bits(self.to_bits() - 1)
            }
        } else { // Self is negative or 0 and moving negative
            // Get positive representation of x and destination
            let source = if *self == float_zero { float_zero } else { -*self };
            let dest = -*y;

            // Return will be the old trick f32 -> u32 +- 1 -> f32 (but we make it negative)
            if dest > source && source >= float_zero {
                -f32::from_bits(source.to_bits() + 1)
            } else {
                -f32::from_bits(source.to_bits() - 1)
            }
        }
    }
}

fn return_invalid_next<T: num_traits::Float>(x: &T, y: &T) -> Option<T> {
    // If x and y are equal (also -0_f64 == 0_f64 in rust), there is nothing further to do
    if *y == *x {
        return Some(*y);
    }

    // If x or y is NaN return NaN
    if x.is_nan() || y.is_nan() {
        return Some(T::nan());
    }

    // If x is infinite or somehow greater
    if *x >= T::infinity() {
        return Some(T::infinity());
    }

    // If x is negative infinite or somehow lower
    if *x <= T::neg_infinity() {
        return Some(T::neg_infinity());
    }

    None
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn next_larger_than_0() {
        let zero = 0_f64;
        let next = zero.next_after(&std::f64::INFINITY);
        assert_eq!(next, 0.000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000005_f64);
    }

    #[test]
    fn next_smaller_than_0() {
        let zero = 0_f64;
        let next = zero.next_after(&std::f64::NEG_INFINITY);
        let answer = -0.000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000005;
        assert_eq!(next, answer);
    }

    #[test]
    fn next_larger_than_a_big_number() {
        let big_num = 16237485966.00000437586943_f64;
        let next = big_num.next_after(&std::f64::INFINITY);
        assert_eq!(next, 16237485966.000006_f64);
    }

    #[test]
    fn next_smaller_than_a_small_number() {
        let small_num = -16237485966.00000437586943_f64;
        let next = small_num.next_after(&std::f64::NEG_INFINITY);
        assert_eq!(next, -16237485966.000006_f64);
    }

    #[test]
    fn next_larger_than_1() {
        let one = 1_f64;
        let next = one.next_after(&std::f64::INFINITY);
        assert_eq!(next, 1_f64 + std::f64::EPSILON);
    }

    #[test]
    fn next_smaller_than_1() {
        let one = 1_f64;
        let next = one.next_after(&std::f64::NEG_INFINITY);
        assert_eq!(next, 0.9999999999999999_f64);
    }

    #[test]
    fn next_smaller_than_neg_1() {
        let neg_one = -1_f64;
        let next = neg_one.next_after(&std::f64::NEG_INFINITY);
        assert_eq!(next, -1_f64 - std::f64::EPSILON);
    }

    #[test]
    fn next_larger_than_neg_1() {
        let neg_one = -1_f64;
        let next = neg_one.next_after(&std::f64::INFINITY);
        assert_eq!(next, -0.9999999999999999_f64);
    }

    #[test]
    fn next_larger_than_0_f32() {
        let zero = 0_f32;
        let next = zero.next_after(&std::f32::INFINITY);
        assert_eq!(next, 0.000000000000000000000000000000000000000000001_f32);
    }

    #[test]
    fn next_smaller_than_0_f32() {
        let zero = 0_f32;
        let next = zero.next_after(&std::f32::NEG_INFINITY);
        let answer = -0.000000000000000000000000000000000000000000001_f32;
        assert_eq!(next, answer);
    }

    #[test]
    fn next_larger_than_a_big_number_f32() {
        let big_num = 16237486000.0_f32;
        let next = big_num.next_after(&std::f32::INFINITY);
        assert_eq!(next, 16237487000.0_f32);
    }

    #[test]
    fn next_smaller_than_a_small_number_f32() {
        let small_num = -16237486000.0_f32;
        let next = small_num.next_after(&std::f32::NEG_INFINITY);
        assert_eq!(next, -16237487000.0_f32);
    }

    #[test]
    fn next_larger_than_1_f32() {
        let one = 1_f32;
        let next = one.next_after(&std::f32::INFINITY);
        assert_eq!(next, 1_f32 + std::f32::EPSILON);
    }

    #[test]
    fn next_smaller_than_1_f32() {
        let one = 1_f32;
        let next = one.next_after(&std::f32::NEG_INFINITY);
        assert_eq!(next, 0.99999994_f32);
    }

    #[test]
    fn next_smaller_than_neg_1_f32() {
        let neg_one = -1_f32;
        let next = neg_one.next_after(&std::f32::NEG_INFINITY);
        assert_eq!(next, -1_f32 - std::f32::EPSILON);
    }

    #[test]
    fn next_larger_than_neg_1_f32() {
        let neg_one = -1_f32;
        let next = neg_one.next_after(&std::f32::INFINITY);
        assert_eq!(next, -0.99999994_f32);
    }

    #[test]
    fn returns_nan() {
        let nan = f64::NAN;
        let next = nan.next_after(&0_f64);
        assert!(next.is_nan());
    }

    #[test]
    fn returns_with_dest_nan() {
        let nan = 8594.77774_f64;
        let next = nan.next_after(&f64::NAN);
        assert!(next.is_nan());
    }

    #[test]
    fn returns_infinity() {
        let infinity = f64::INFINITY;
        let next = infinity.next_after(&0_f64);
        assert!(next.is_infinite());
        assert_eq!(next, f64::INFINITY);
    }

    #[test]
    fn returns_neg_infinity() {
        let neg_infinity = f64::NEG_INFINITY;
        let next = neg_infinity.next_after(&0_f64);
        assert!(next.is_infinite());
        assert_eq!(next, f64::NEG_INFINITY);
    }

    #[test]
    fn returns_equal_values() {
        let eq_1 = 1345.9875646_f64;
        let next = eq_1.next_after(&1345.9875646_f64);
        assert_eq!(next, 1345.9875646_f64);
    }

    #[test]
    fn returns_equal_negative_and_positive_zero() {
        let zero = 0_f64;
        let next = zero.next_after(&-0_f64);
        assert_eq!(next, -0_f64);
    }

    #[test]
    fn returns_equal_negative_and_positive_zero_f32() {
        let zero = 0_f32;
        let next = zero.next_after(&-0_f32);
        assert_eq!(next, -0_f32);
    }
}
